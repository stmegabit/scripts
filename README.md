# Скрипты для Ubuntu Server 
## Требования
Для запуска скриптов на удалённом сервере понадобится утилита cUrl. Все скрипты необходимо выполнять от имени
суперпользователя (root).

Проверить наличие cUrl:
```bash
curl --version
```

Установить cUrl:
```bash
apt-get install curl -y
```

## Скрипты
Базовая инициализация:
```bash
curl -s https://bitbucket.org/stmegabit/scripts/raw/master/bash/init.sh | bash
```

Установка Docker CE (Ubuntu 16.04):
```bash
curl -s https://bitbucket.org/stmegabit/scripts/raw/master/bash/install-docker-ubuntu-xenial.sh | bash
```

Очистка:
```bash
curl -s https://bitbucket.org/stmegabit/scripts/raw/master/bash/cleanup.sh | bash
```