#!/usr/bin/env bash

apt-get update;
apt-get upgrade -y;

locale-gen ru_RU.UTF-8;

apt-get install htop vim -y;
